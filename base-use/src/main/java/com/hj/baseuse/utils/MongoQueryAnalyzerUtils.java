package com.hj.baseuse.utils;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/** 查看MongoDB 执行计划信息
 * @author: hejie
 * @date: 2024/12/8
 */
@Component
//@Slf4j
public class MongoQueryAnalyzerUtils {

    //调试日志
    private final static Logger log = LoggerFactory.getLogger(MongoQueryAnalyzerUtils.class);


    @Resource
    private MongoTemplate mongoTemplate;

    public Document analyzeQuery(String collectionName, Query query) {
        try {
            // 获取执行计划
            Document explainOutput = getQueryExplainPlan(collectionName, query);
            log.info("执行计划 explainResult:{}", explainOutput.toJson());
            // 分析执行计划
            analyzeExplainPlan(explainOutput);
            // 如果发现性能问题，给出优化建议
            provideSuggestions(explainOutput);
            return explainOutput;
        } catch (Exception e) {
            log.error("Failed to analyze query for collection: {}", collectionName, e);
            throw new RuntimeException("查询分析失败", e);
        }
    }

    private Document getQueryExplainPlan(String collectionName, Query query) {
        // 使用不同的执行计划模式
        // queryPlanner: 显示查询优化器选择的计划
        // executionStats: 包含实际执行统计信息
        // allPlansExecution: 显示所有考虑的计划
        // 获取执行计划
        Document explainCommand = new Document();
        explainCommand.put("explain", new Document("find", collectionName)
                .append("filter", query.getQueryObject()));

        // 设置 verbosity 模式，可选值：queryPlanner(默认)、executionStats、allPlansExecution
        explainCommand.put("verbosity", "allPlansExecution");

        // 获取执行计划
        Document explainOutput = mongoTemplate.executeCommand(explainCommand);
        return explainOutput;
//        return mongoTemplate.getCollection(collectionName)
//                .find(query.getQueryObject())
//                .explain(new Document("verbosity", "allPlansExecution"));
    }

    private void analyzeExplainPlan(Document explainOutput) {
        // 提取关键执行信息
        Document queryPlanner = (Document) explainOutput.get("queryPlanner");
        Document winningPlan = (Document) queryPlanner.get("winningPlan");
        Document executionStats = (Document) explainOutput.get("executionStats");

        // 分析查询计划类型
        String queryStage = winningPlan.getString("stage");

        // 分析是否使用了索引
        boolean usedIndex = !"COLLSCAN".equals(queryStage);

        if (executionStats != null) {
            // 分析执行统计
            int nReturned = executionStats.getInteger("nReturned");
            int totalKeysExamined = executionStats.getInteger("totalKeysExamined");
            int totalDocsExamined = executionStats.getInteger("totalDocsExamined");
            int executionTimeMillis = executionStats.getInteger("executionTimeMillis");

            logQueryPerformance(queryStage, usedIndex, nReturned,
                    totalKeysExamined, totalDocsExamined, executionTimeMillis);
        }
    }

    private void logQueryPerformance(String queryStage, boolean usedIndex,
                                     int nReturned, int totalKeysExamined, int totalDocsExamined,
                                     int executionTimeMillis) {
        StringBuilder analysis = new StringBuilder();
        analysis.append("查询执行计划分析:\n");
        analysis.append("执行阶段: ").append(queryStage).append("\n");
        analysis.append("是否使用索引: ").append(usedIndex).append("\n");
        analysis.append("返回文档数: ").append(nReturned).append("\n");
        analysis.append("检查索引条目数: ").append(totalKeysExamined).append("\n");
        analysis.append("检查文档数: ").append(totalDocsExamined).append("\n");
        analysis.append("执行时间(毫秒): ").append(executionTimeMillis).append("\n");

        // 计算查询效率指标
        double documentsScanRatio = (double) totalDocsExamined / nReturned;
        analysis.append("文档扫描比率: ").append(String.format("%.2f", documentsScanRatio)).append("\n");

        log.info(analysis.toString());
    }

    private void provideSuggestions(Document explainOutput) {
        List<String> suggestions = new ArrayList<>();
        Document queryPlanner = (Document) explainOutput.get("queryPlanner");
        Document winningPlan = (Document) queryPlanner.get("winningPlan");
        Document executionStats = (Document) explainOutput.get("executionStats");

        // 检查是否进行了全表扫描
        if ("COLLSCAN".equals(winningPlan.getString("stage"))) {
            suggestions.add("查询执行了全表扫描，建议创建适当的索引");
        }

        // 检查索引效率
        if (executionStats != null) {
            int nReturned = executionStats.getInteger("nReturned");
            int totalDocsExamined = executionStats.getInteger("totalDocsExamined");

            if (totalDocsExamined > nReturned * 3) {
                suggestions.add("查询扫描的文档数远大于返回的文档数，建议优化索引或查询条件");
            }
        }

        // 检查内存使用
        if (winningPlan.containsKey("stage") &&
                "SORT".equals(winningPlan.getString("stage"))) {
            suggestions.add("查询包含内存中排序，建议在排序字段上创建索引");
        }

        // 输出优化建议
        if (!suggestions.isEmpty()) {
            log.info("查询优化建议:");
            suggestions.forEach(suggestion -> log.info("- " + suggestion));
        }
    }

    public void analyzeAggregation(String collectionName,
                                   AggregationOperation... operations) {
        try {
            Aggregation aggregation = Aggregation.newAggregation(operations);

            // 获取聚合管道的执行计划
            Document explainOutput = mongoTemplate.aggregate(aggregation,
                            collectionName, Document.class)
                    .getRawResults().get("$explain", Document.class);

            // 分析聚合管道的执行计划
            analyzeAggregationPlan(explainOutput);

        } catch (Exception e) {
            log.error("Failed to analyze aggregation for collection: {}",
                    collectionName, e);
            throw new RuntimeException("聚合分析失败", e);
        }
    }

    private void analyzeAggregationPlan(Document explainOutput) {
        StringBuilder analysis = new StringBuilder();
        analysis.append("聚合管道执行计划分析:\n");

        // 分析各个阶段的执行情况
        List<Document> stages = (List<Document>) explainOutput.get("stages");
        if (stages != null) {
            for (int i = 0; i < stages.size(); i++) {
                Document stage = stages.get(i);
                analysis.append("阶段 ").append(i + 1).append(":\n");
                analysis.append("  操作类型: ").append(stage.getString("$stage")).append("\n");

                // 分析时间花费
                if (stage.containsKey("executionTimeMillisEstimate")) {
                    analysis.append("  预估执行时间(毫秒): ")
                            .append(stage.getLong("executionTimeMillisEstimate"))
                            .append("\n");
                }

                // 分析内存使用
                if (stage.containsKey("memUsage")) {
                    analysis.append("  内存使用: ")
                            .append(stage.get("memUsage"))
                            .append("\n");
                }
            }
        }

        log.info(analysis.toString());
    }

    // 示例使用方法
    public void analyzeQueryExample() {
        // 创建查询条件
        Query query = new Query(Criteria.where("age").gte(18)
                .and("status").is("active"))
                .with(Sort.by(Sort.Direction.DESC, "createTime"));

        // 分析查询
        analyzeQuery("users", query);

        // 创建聚合操作
        AggregationOperation match = Aggregation.match(
                Criteria.where("age").gte(18));
        AggregationOperation group = Aggregation.group("status")
                .count().as("total")
                .avg("age").as("avgAge");
        AggregationOperation sort = Aggregation.sort(
                Sort.Direction.DESC, "total");

        // 分析聚合
        analyzeAggregation("users", match, group, sort);
    }
}
