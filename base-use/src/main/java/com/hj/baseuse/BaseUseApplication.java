package com.hj.baseuse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseUseApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseUseApplication.class, args);
    }

}
