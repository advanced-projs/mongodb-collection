package com.hj.baseuse.controller;

import com.hj.baseuse.po.Comment;
import com.hj.baseuse.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController {


    @Autowired
    private CommentService commentService;

    /**
     * 查询所有评论
     */
    @RequestMapping("/find/comment/all")
    public List<Comment> testFindCommentList() {
        List<Comment> commentList = commentService.findCommentList();
        System.out.println(commentList);
        return commentList;
    }

    /**
     * 根据id查询评论
     */
    @RequestMapping("/find/comment/by-id")
    public Comment testFindCommentById(String id) {
        //todo 注意：_id 字段是 ObjectId 类型  1.在 MongoDB客户端软件中，需要这样查询 db.comment.find({_id :ObjectId('66a820ead6ac423494f3f259')});
//        Comment commentById = commentService.findCommentById("66a820ead6ac423494f3f259");
        Comment commentById = commentService.findCommentById(id);
        System.out.println(commentById);
        return commentById;
    }

    /**
     * 新增评论
     */
    @RequestMapping("/action/comment/save")
    public void testSaveComment() {
        Comment comment = new Comment();
        comment.setArticleid("100000");
        comment.setContent("测试添加的数据");
        comment.setCreatedatetime(LocalDateTime.now());
        comment.setUserid("1003");
        comment.setNickname("凯撒大帝");
        comment.setState("1");
        comment.setLikenum(0);
        comment.setReplynum(0);
        comment.setParentid("0");

        commentService.saveComment(comment);

    }

    @RequestMapping("/find/comment/by-parentid")
    public Page<Comment> testFindCommentListByParentid(@RequestParam("parentid")String parentid, @RequestParam("page")int page,
                                                       @RequestParam("size")int size) {
//        Page<Comment> page = commentService.findCommentListByParentid("0", 1, 2);
        Page<Comment> pageComment = commentService.findCommentListByParentid(parentid, page, size);
        System.out.println(pageComment.getTotalElements());
        System.out.println(pageComment.getContent());
        return pageComment;
    }

    /**
     * 更新 点赞数（用mongoTemplate方式更新 ）
     */
    @RequestMapping("/action/comment/increse-likenum")
    public void testUpdateCommentLikenum(String id) {
//        commentService.updateCommentLikenum("66a820ead6ac423494f3f259");
        commentService.updateCommentLikenum(id);
//        commentService.updateCommentLikenum("1");
    }
}
