package com.hj.baseuse.controller;

import com.hj.baseuse.po.Comment;
import com.hj.baseuse.service.CommentService;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/comment/template")
public class CommentTemplateController {


    @Autowired
    private CommentService commentService;
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 查询所有评论
     */
    @RequestMapping("/find/comment/all")
    public List<Comment> testFindCommentList() {
        List<Comment> commentList = mongoTemplate.findAll(Comment.class);
        System.out.println(commentList);
        return commentList;
    }

    /**
     * 根据id查询评论
     */
    @RequestMapping("/find/comment/by-id")
    public Comment testFindCommentById(String id) {
        //todo 注意：_id 字段是 ObjectId 类型  1.在 MongoDB客户端软件中，需要这样查询 db.comment.find({_id :ObjectId('66a820ead6ac423494f3f259')});
//        Comment commentById = commentService.findCommentById("66a820ead6ac423494f3f259");
        Criteria criteria1 = Criteria.where("_id").is(id);
//        Criteria criteria2 = Criteria.where("update_ts").lte(System.currentTimeMillis());
        Query query = new Query();
        query.addCriteria(criteria1);
//        query.addCriteria(criteria2);
        final Comment commentById = mongoTemplate.findOne(query, Comment.class);
        System.out.println(commentById);
        return commentById;
    }

    /**
     * 新增评论
     */
    @RequestMapping("/action/comment/save")
    public void testSaveComment() {
        Comment comment = new Comment();
        comment.setArticleid("100001");
        comment.setContent("测试添加的数据template");
        comment.setCreatedatetime(LocalDateTime.now());
        comment.setUserid("1005");
        comment.setNickname("刘备");
        comment.setState("1");
        comment.setLikenum(0);
        comment.setReplynum(0);
        comment.setParentid("0");
        mongoTemplate.save(comment);
//        mongoTemplate.insertAll(Arrays.asList(comment));
    }
    
    /**
     * 批量新增评论
     */
    @RequestMapping("/action/comment/save-batch")
    public void testSaveBatchComment() {
        Comment comment = buildComment();
        Comment comment2 = buildComment();
        comment2.setNickname("关羽");
        mongoTemplate.insertAll(Arrays.asList(comment, comment2));
    }

    /**
     * 分页、排序
     * @param parentid
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/find/comment/by-parentid")
    public List<Comment> testFindCommentListByParentid(@RequestParam("parentid")String parentid, @RequestParam("page")int page,
                                                       @RequestParam("size")int size) {
        Criteria criteria1 = Criteria.where("parentid").is(parentid);
        //第一种分页方式
//        Pageable pageable = PageRequest.of(page, size);

        //hj 注意： MongoDB集合 水平分表后，不能使用PageRequest方式，不然查询不出来数据
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "_id"));
        Query query = new Query();
        query.addCriteria(criteria1)
                .with(pageable);
        //第二种 分页方式
//        query.limit(5000).skip(5000);
        List<Comment> pageComment = mongoTemplate.find(query, Comment.class);
        System.out.println(pageComment);
        return pageComment;
    }

    /**
     * 更新 点赞数（用mongoTemplate方式更新 ）
     */
    @RequestMapping("/action/comment/increse-likenum")
    public void testUpdateCommentLikenum(String id) {
        commentService.updateCommentLikenum(id);
    }

    /**
     * 更新 （用mongoTemplate方式更新 ）
     */
    @RequestMapping("/action/comment/update")
    public UpdateResult testUpdateComment(String id) {
        Query query = Query.query(Criteria.where("_id").is(id));
        Update update = Update.update("replynum", 2)
                .set("state", "2");
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Comment.class);
        return updateResult;
    }

    /**
     * 删除
     */
    @RequestMapping("/action/comment/remove-by-id")
    public DeleteResult testRemoveComment(String id) {
        //方式1
        /*Query query = Query.query(Criteria.where("_id").is(id));
        DeleteResult deleteResult = mongoTemplate.remove(query, Comment.class);*/
        //方式2
        Comment deleteComment = new Comment();
        deleteComment.setId(id);
        DeleteResult deleteResult = mongoTemplate.remove(deleteComment);

        return deleteResult;
    }

    private Comment buildComment() {
        Comment comment = new Comment();
        comment.setArticleid("100001");
        comment.setContent("测试添加的数据template");
        comment.setCreatedatetime(LocalDateTime.now());
        comment.setUserid("1005");
        comment.setNickname("刘备");
        comment.setState("1");
        comment.setLikenum(0);
        comment.setReplynum(0);
        comment.setParentid("0");
        return comment;
    }
}
